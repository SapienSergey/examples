var $ajaxBlock = null;
var paymentSystemInput = $("input[name='paymentSystem']");
var paymentSystemInputs = $("div[data-payment-methods]");
var purchaseButton = $("button[data-purchase]");
var purchaseModal = $("div#purchaseModal");
var purchaseModalMonthsSelect = purchaseModal.find("select[name='months']");
var purchaseModalTable = purchaseModal.find("tbody");
var purchaseModalTax = purchaseModal.find("td[data-tax]");
var purchaseModalTaxGST = purchaseModal.find("td[data-tax-gst]");
var purchaseModalTaxHST = purchaseModal.find("td[data-tax-hst]");
var purchaseModalTaxQST = purchaseModal.find("td[data-tax-qst]");
var purchaseModalPrice = purchaseModal.find("td[data-price]");
var purchaseModalConfirmBtn = purchaseModal.find("button[data-confirm]");
var purchaseModalErrorBlock = purchaseModal.find("div.alert-danger");

$.ajaxSetup({
    beforeSend: function(){
        $('.x_panel_loader').show();
    },
    complete: function(){
        $('.x_panel_loader').hide();
    }
});

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function parseTable(details) {
    var transaction = details.transaction;
    var taxes = details.taxes;
    var tax_GST = taxes.gst;
    var tax_HST = taxes.hst;
    var tax_QST = taxes.qst;
    purchaseModalTable.html(null);
    $.each(transaction.item_list.items, function (k, item) {
        var tr = $("<tr>");
        tr.append($("<td>").html(k+1)).append($("<td>").html(item.name)).append($("<td>").html(addCommas(parseFloat(item.price).toFixed(2))+" "+item.currency));
        purchaseModalTable.append(tr);
    });
    purchaseModalTax.find("strong").html(addCommas(parseFloat(transaction.amount.details.tax).toFixed(2))+' '+transaction.amount.currency);
    purchaseModalPrice.find("strong").html(addCommas(parseFloat(transaction.amount.total).toFixed(2))+' '+transaction.amount.currency);
    if (tax_GST > 0) {
        purchaseModalTaxGST.find("strong").html(addCommas(parseFloat(tax_GST).toFixed(2))+' '+transaction.amount.currency);
    }if (tax_HST > 0) {
        purchaseModalTaxHST.find("strong").html(addCommas(parseFloat(tax_HST).toFixed(2))+' '+transaction.amount.currency);
    }if (tax_QST > 0) {
        purchaseModalTaxQST.find("strong").html(addCommas(parseFloat(tax_QST).toFixed(2))+' '+transaction.amount.currency);
    }
}

purchaseModalConfirmBtn.on('click', function (e) {
    $ajaxBlock = purchaseModalConfirmBtn;
    var url = $ajaxBlock.data('url');
    var months = purchaseModalMonthsSelect.val();
    purchaseModalErrorBlock.html(null);
    purchaseModalErrorBlock.addClass('hidden');
    purchaseModal.modal('hide');
    $.post(url, {months: months, paymentSystem: $(paymentSystemInputs.find('label.active')).find('input').val()}, function (res) {
        if (isUrlValid(res)) {
            window.location.replace(res);
        }
    }).fail(function (res, error, message) {
        purchaseModal.modal('show');
        purchaseModalErrorBlock.removeClass('hidden');
        purchaseModalErrorBlock.append($("<p>").html(message));
        $.each(res.responseJSON, function (k, error) {
            purchaseModalErrorBlock.append($("<p>").html(error));
        });
    });
});

purchaseModalMonthsSelect.on('change', function (e) {
    var months = $(this).val();
    purchaseModalErrorBlock.html(null);
    purchaseModalErrorBlock.addClass('hidden');
    getPayPalInfo(months, function (res) {
        parseTable(res);
    }, function (res, error, message) {
        purchaseModalErrorBlock.removeClass('hidden');
        purchaseModalErrorBlock.html($("<p>").html(message));
    });
});

purchaseButton.on('click', function (e) {
    purchaseModal.modal('show');
    purchaseModalErrorBlock.html(null);
    purchaseModalErrorBlock.addClass('hidden');
    getPayPalInfo(purchaseModalMonthsSelect.val(), function (res) {
        parseTable(res);
    }, function (res, error, message) {
        if (res !== undefined && res.responseJSON !== undefined) {
            message += "<br>" + res.responseJSON;
        }
        purchaseModalErrorBlock.removeClass('hidden');
        purchaseModalErrorBlock.html($("<p>").html(message));
    });
});

paymentSystemInput.on('change', function (e) {
    e.preventDefault();
    purchaseButton.removeClass('hidden');
});

// paymentSystemInput.trigger('change');

function getPayPalInfo(months, success, error)
{
    $ajaxBlock = purchaseButton;
    var url = $ajaxBlock.data('url');
    $.get(url, {months: months}, function (res) {
        success(res);
    }).fail(function (xhr, err, errorMsg) {
        error(xhr, err, errorMsg);
    });
}