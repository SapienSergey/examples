/**
 * Created by Sergey Korovkin on 21.09.2016.
 */
'use strict';

$(function(){
    var form = $("form[data-filter]");
    var inputs = form.find("input[data-type='autocomplete']");

    if (form !== undefined && form.length === 1) {
        var fields = [];
        var submitBtn = form.find("button[data-type='filter']");
        var clearBtn = form.find("button[data-type='clear']");
        var defaultValue = $(submitBtn).data('default-value');
        var errorBlock = $("div[data-filter-error]");
        $.each(inputs, function (k, input) {
            var fieldName = $(input).data('field');
            var sourceURL = $(input).data('source');
            var filterInput = form.find("input[name='filter["+fieldName+"]']");
            if ($.inArray(fieldName, fields) === -1) {
                fields.push(fieldName);
            }
            $(input).autocomplete({
                source: sourceURL,
                minLength: 0,
                select: function( event, ui ) {
                    $(filterInput).val(ui.item.id);
                    $(input).data('selected', ui.item.value);
                },
                search: function (event, ui) {
                    if ($(input).data('selected') !== $(input).val()) {
                        filterInput.val(null);
                    }
                    var searchUrl = sourceURL+'&'+form.serialize();
                    $(input).autocomplete( "option", "source", searchUrl );
                    $.each(inputs, function (k, _input) {
                        if (_input != input) {
                            $(_input).attr('disabled', true);
                        }
                    });
                },
                response: function (event, ui) {
                    $.each(inputs, function (k, _input) {
                        if (_input != input) {
                            $(_input).attr('disabled', false);
                        }
                    });
                },
                close: function (event, ui) {
                    var autocompleteInputValue = $(input).val();
                    if (autocompleteInputValue === undefined || autocompleteInputValue.length === 0) {
                        $(input).val(defaultValue.value);
                        $(filterInput).val(defaultValue.id);
                    }
                    var filterInputValue = $(filterInput).val();
                    if (filterInputValue === undefined || filterInputValue.length === 0) {
                        var formGroupElement = $(filterInput).parents("div.form-group");
                        var fieldLabel = formGroupElement.find("div.input-group-addon");
                        var alertBlock = errorBlock.find('div.alert');
                        var errorText = errorBlock.data('filter-error');
                        var p = $("<p>");
                        $.each(inputs, function (k, _input) {
                            if (_input != input) {
                                $(_input).attr('disabled', true);
                            }
                        });
                        alertBlock.html(null);
                        p.html(fieldLabel.html()+": "+errorText);
                        alertBlock.append(p);
                        $(formGroupElement).addClass('has-error');
                        errorBlock.removeClass('hidden');
                    }
                }
            });
            $(input).on( "click", function( event, ui ) {
                var fieldVal = filterInput.val();
                var searchText = '';
                var formGroupElement = $(filterInput).parents("div.form-group");
                $(formGroupElement).removeClass('has-error');
                errorBlock.addClass('hidden');
                $(this).select();
                if (parseInt(fieldVal) !== -1) {
                    searchText = $(this).val();
                } else {
                    $(input).val(null);
                }
                $(input).autocomplete("search", searchText);
            });
        });
        submitBtn.on('click', function (e) {
            var errorFields = [];
            var alertBlock = errorBlock.find('div.alert');
            var errorText = errorBlock.data('filter-error');
            alertBlock.html(null);
            $.each(inputs, function (k, input) {
                var fieldName = $(input).data('field');
                var filterInput = form.find("input[name='filter["+fieldName+"]']");
                var formGroupElement = $(filterInput).parents("div.form-group");
                var fieldLabel = formGroupElement.find("div.input-group-addon");
                $(formGroupElement).removeClass('has-error');
                if (filterInput.val().length === 0) {
                    $(formGroupElement).addClass('has-error');
                    errorFields.push(fieldLabel.html());
                    var p = $("<p>");
                    p.html(fieldLabel.html()+": "+errorText);
                    alertBlock.append(p);
                }
            });
            if (errorFields.length > 0) {
                errorBlock.removeClass('hidden');
            } else {
                errorBlock.addClass('hidden');
            }
            return errorFields.length === 0;
        });
        clearBtn.on('click', function (e) {
            $.each(inputs, function (k, input) {
                var fieldName = $(input).data('field');
                var filterInput = form.find("input[name='filter["+fieldName+"]']");
                $(input).val(defaultValue.value);
                $(filterInput).val(defaultValue.id);
            });
        });
    }
});
