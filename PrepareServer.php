<?php

namespace GTH\UserBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use GTH\CoreBundle\Controller\Util\CurrencyTrait;
use GTH\CoreBundle\Controller\Util\InputTrait;
use GTH\CoreBundle\Controller\Util\LanguageTrait;
use GTH\CoreBundle\Entity\Currency;
use GTH\CoreBundle\Entity\Language;
use GTH\CoreBundle\Entity\Order;
use GTH\CoreBundle\Entity\Payment;
use GTH\CoreBundle\Entity\PaymentOrder;
use GTH\CoreBundle\Entity\PaymentSystem;
use GTH\CoreBundle\Entity\Region;
use GTH\CoreBundle\Entity\Server\Bandwidth;
use GTH\CoreBundle\Entity\Server\CPU;
use GTH\CoreBundle\Entity\Server\CPUInstalled;
use GTH\CoreBundle\Entity\Server\CPURac;
use GTH\CoreBundle\Entity\Server\HDD;
use GTH\CoreBundle\Entity\Server\HDDInstalled;
use GTH\CoreBundle\Entity\Server\HDDRac;
use GTH\CoreBundle\Entity\Server\IP;
use GTH\CoreBundle\Entity\Server\OperationSystem;
use GTH\CoreBundle\Entity\Server\RAC;
use GTH\CoreBundle\Entity\Server\RAM;
use GTH\CoreBundle\Entity\Server\RAMInstalled;
use GTH\CoreBundle\Entity\Server\RAMRac;
use GTH\CoreBundle\Entity\Server\Server;
use GTH\CoreBundle\Entity\Service;
use GTH\CoreBundle\Entity\ServicePrice;
use GTH\CoreBundle\Entity\StaticPage;
use GTH\CoreBundle\Entity\User;
use GTH\CoreBundle\Entity\Util\BaseEntity;
use GTH\CoreBundle\Entity\Voucher\Voucher;
use GTH\CoreBundle\Entity\Voucher\VoucherHistory;
use GTH\CoreBundle\Entity\Voucher\VoucherPrice;
use GTH\CoreBundle\Repository\CurrencyRepository;
use GTH\CoreBundle\Repository\PaymentSystemRepository;
use GTH\CoreBundle\Repository\RegionRepository;
use GTH\CoreBundle\Repository\ServerRepository;
use GTH\CoreBundle\Repository\ServiceRepository;
use GTH\CoreBundle\Repository\StaticPageRepository;
use GTH\CoreBundle\Repository\UserRepository;
use GTH\CoreBundle\Repository\Util\ListResult;
use GTH\CoreBundle\Repository\VoucherRepository;
use GTH\CoreBundle\Services\EmailSender;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Validator\ConstraintViolation;

class CustomServerController extends Controller
{
    use InputTrait;
    use LanguageTrait;
    use CurrencyTrait;

    public function indexAction(Request $request){
        $referer = $request->headers->get('referer');
        $defaultLocale = $this->container->getParameter('locale');
        $locale = $defaultLocale;

        if ($referer !== null) {
            $urlArray = explode('/', $referer);
            $host = implode('/', [$urlArray[0],$urlArray[1],$urlArray[2]]);
            $prodHost = $this->container->getParameter('production_url');
            $locales = $this->container->getParameter('locales');
            $locales = explode('|', substr($locales, 1, (strlen($locales) - 2)));
            if ($prodHost === $host) {
                $locale = array_key_exists(3, $urlArray) ? $urlArray[3] : $defaultLocale;
                if (!in_array($locale, $locales)) {
                    $locale = $defaultLocale;
                }
            }
        }

        return $this->redirectToRoute('gth_user_custom_server_prepare', ['_locale' => $locale]);
    }

    public function prepareAction(Request $request)
	{
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Translator $translator */
        $translator = $this->get('translator');
        /** @var SessionInterface $session */
        $session = $request->getSession();

        //clear session data
        $session->remove('region');
        $session->remove('rac');
        $session->remove('currency');
        $session->remove('cpuService');
        $session->remove('ramService');
        $session->remove('hddService');
        $session->remove('osService');
        $session->remove('portService');
        $session->remove('ipService');
        $session->remove('voucher');

        /** @var ServerRepository $ipRepository */
        $ipRepository = $em->getRepository('GTHCoreBundle:Server\IP');
        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $em->getRepository('GTHCoreBundle:Currency');
        /** @var RegionRepository $regionRepository */
        $regionRepository = $em->getRepository('GTHCoreBundle:Region');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $em->getRepository('GTHCoreBundle:Service');
        /** @var ServerRepository $racRepository */
        $racRepository = $em->getRepository('GTHCoreBundle:Server\RACRegion');

         $order = new Order();
        if ($request->isMethod(Request::METHOD_POST)) {
            $regionId = (int)$request->request->get('region', 0);
            $currencyId = (int)$request->request->get('currency', 0);
            $racId = (int)$request->request->get('rac', 0);
            $cpuServiceId = (int)$request->request->get('cpuService', 0);
            $hddServiceId = (int)$request->request->get('hddService', 0);
            $ramServiceId = (int)$request->request->get('ramService', 0);
            $osServiceId = (int)$request->request->get('osService', 0);
            $portServiceId = (int)$request->request->get('portService', 0);
            $ipServiceId = (int)$request->request->get('ipService', -1);
            $next = $request->request->get('next', null);
            /** @var Language $language */
            $language = $this->getCurrentLanguage($request);
            /** @var Language $defaultLanguage */
            $defaultLanguage = $this->getCurrentLanguage($request, $this->getParameter('locale'));
            /** @var Currency[] $currencies */
            $currencies = $request->get('currencies');
            /** @var Region[] $regions */
            $regions = $regionRepository->findBy(['active' => true], ['sortOrder' => Criteria::ASC]);
            foreach ($regions as $key => $_reg) {
                $racsInRegion = count($racRepository->getListByRegion($_reg, new RAC(), 'rac'));
                if ($racsInRegion === 0) {
                    unset($regions[$key]);
                }
            }
            if (count($regions) === 0 or count($currencies) === 0) {
                return new JsonResponse($translator->trans('user.server.session.data.missing.error').'1', 409);
            }
            /** @var Currency $currency */
            $currency = $this->getEntityById($currencies, $currencyId);
            /** @var Region $region */
            $region = $regionRepository->findOneBy(['id' => $regionId,'active' => true]);
            if (!$region instanceof Region) {
                $region = reset($regions) ?: null;
            }
            if (!$region instanceof Region) {
                return new JsonResponse($translator->trans('user.server.session.data.missing.error').'2', 409);
            }
            /** @var RAC[] $racs */
            $racs = $racRepository->getListByRegion($region, new RAC(), 'rac', true, Criteria::ASC);
            $osService = new Service();
            $portService = new Service();
            $ipService = null;
            $cpuService = new Service();
            $hddService = new Service();
            $ramService = new Service();
            $serverIPMIip = new IP();
            $serverIP = new IP();
            $rac = new RAC();
            $osServices = [];
            $portServices = [];
            $ipServices = [];
            $cpuServices = [];
            $hddServices = [];
            $ramServices = [];
            $requiredServices = [];
            $server = (new Server())->prepareCustomServer($region, $currency, $defaultLanguage, $em, $rac, $cpuService, $hddService, $ramService, $osService, $portService, $serverIP, $serverIPMIip);
            if (count($racs) > 0) {
                /** @var RAC $rac */
                $rac = $this->getEntityById($racs, $racId);
                /** @var ListResult $servicesList */
                $servicesList = $serviceRepository->getListByRegion($region, $language, null, null, ['sortOrder' => Criteria::ASC], [
                    ["active", 'eq', true],
                    ["orx", 'orxX', [
                        ['hcr.rac', 'eq', $rac->id],
                        ['hrr.rac', 'eq', $rac->id],
                        ['hhr.rac', 'eq', $rac->id]
                    ]]
                ]);
                /** @var Service[] $services */
                $services = $servicesList->getResult();
                /** @var Service[] $cpuServices */
                $cpuServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_CPU, $currency, $region, $rac);
                /** @var Service[] $hddServices */
                $hddServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_HDD, $currency, $region, $rac);
                /** @var Service[] $ramServices */
                $ramServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_RAM, $currency, $region, $rac);
                /** @var Service[] $osServices */
                $osServices = $this->sortServicesByTypes($services, Service::TYPE_SOFTWARE, Service::SUBTYPE_OSES, $currency, null);
                /** @var Service[] $ipServices */
                $ipServices = $this->sortServicesByTypes($services, Service::TYPE_NETWORK, Service::SUBTYPE_IP, $currency, $region);
                /** @var Service[] $portServices */
                $portServices = $this->sortServicesByTypes($services, Service::TYPE_NETWORK, Service::SUBTYPE_BANDWIDTH, $currency, $region);
                /** @var Service[] $requiredServices */
                $requiredServices = $this->sortServicesByTypes($services, Service::TYPE_OTHER, Service::SUBTYPE_REQUIRED, $currency, $region);

                /** @var ListResult $freeIPList */
                $freeIPList = $ipRepository->getList(new IP(), null, null, ["id" => Criteria::DESC], [
                    "active" => false,
                    "public" => true,
                    "ipmi" => false,
                    "order" => null,
                    "v.region" => $region->id
                ]);
                //TODO: REMOVE WHEN IPMI IP WILL BE NOT REQUIRED FOR SERVER
                /** @var ListResult $freeIPMIIPList */
                $freeIPMIIPList = $ipRepository->getList(new IP(), null, null, ["id" => Criteria::DESC], [
                    "active" => false,
                    "public" => false,
                    "ipmi" => true,
                    "order" => null,
                    "v.region" => $region->id
                ]);
                /** @var int $countFreeIp */
                $countFreeIp = $freeIPList->getCount();
                //TODO: REMOVE WHEN IPMI IP WILL BE NOT REQUIRED FOR SERVER
                /** @var int $countFreeIPMIIp */
                $countFreeIPMIIp = $freeIPMIIPList->getCount();
                //clear ips
                foreach ($ipServices as $key => $_ipService) {
                    if ($_ipService->count > $countFreeIp) {
                        unset($ipServices[$key]);
                    }
                }
                /** @var IP[] $freeIPs */
                $freeIPs = $freeIPList->getResult();
                /** @var IP[] $freeIPMIips */
                $freeIPMIips = $freeIPMIIPList->getResult();

                if (count($cpuServices) > 0) {
                    /** @var Service $cpuService */
                    $cpuService = $this->getEntityById($cpuServices, $cpuServiceId);
                }
                if (count($hddServices) > 0) {
                    /** @var Service $hddService */
                    $hddService = $this->getEntityById($hddServices, $hddServiceId);
                }
                if (count($ramServices) > 0) {
                    /** @var Service $ramService */
                    $ramService = $this->getEntityById($ramServices, $ramServiceId);
                }
                if (count($osServices) > 0) {
                    /** @var Service $osService */
                    $osService = $this->getEntityById($osServices, $osServiceId);
                }
                if (count($portServices) > 0) {
                    /** @var Service $portService */
                    $portService = $this->getEntityById($portServices, $portServiceId);
                }
                if ($countFreeIp > 0) {
                    /** @var IP $serverIP */
                    $serverIP = $freeIPs[0];
                }
                if ($countFreeIPMIIp > 0) {
                    /** @var IP $serverIPMIip */
                    $serverIPMIip = $freeIPMIips[0];
                }
                if (count($ipServices) > 0) {
                    /** @var Service|null $ipService */
                    $ipService = $ipServiceId >= 0 ? $this->getEntityById($ipServices, $ipServiceId) : null;
                }

                /** @var Service|null $otherService */
                $otherService = null;

                $server = (new Server())->prepareCustomServer($region, $currency, $defaultLanguage, $em, $rac, $cpuService, $hddService, $ramService, $osService, $portService, $serverIP, $serverIPMIip);
                /** @var Order $order */
                $order = $server->getMainOrder();

                foreach ($requiredServices as $requiredService) {
                    $requiredServiceOrder = new Order();
                    $requiredServiceOrder->status = Order::STATUS_PENDING;
                    $requiredServiceOrder->currency = $order->currency;
                    $requiredServiceOrder->server = $server;
                    $requiredServiceOrder->service = $requiredService;
                    $requiredServiceOrder->date_expired = null;
                    $requiredServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                    $requiredServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                    $requiredServiceOrder->description = $requiredService->getTranslates($language)->name;
                    $em->persist($requiredServiceOrder);
                    $requiredServiceOrder->setName($requiredServiceOrder->id);
                    /** @var Server $server */
                    $server = $server->addOrder($requiredServiceOrder);
                }

                if ((float)$portService->getPrices($order->currency, $server->region, true)->price > 0.00) {
                    $portServiceOrder = new Order();
                    $portServiceOrder->status = Order::STATUS_PENDING;
                    $portServiceOrder->currency = $order->currency;
                    $portServiceOrder->server = $server;
                    $portServiceOrder->service = $portService;
                    $portServiceOrder->date_expired = null;
                    $portServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                    $portServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                    $portServiceOrder->description = $portService->getTranslates($language)->name;
                    $em->persist($portServiceOrder);
                    $portServiceOrder->setName($portServiceOrder->id);
                    /** @var Server $server */
                    $server = $server->addOrder($portServiceOrder);
                }

                if ((float)$osService->getPrices($order->currency, $server->region, true)->price > 0.00) {
                    $osServiceOrder = new Order();
                    $osServiceOrder->status = Order::STATUS_PENDING;
                    $osServiceOrder->currency = $order->currency;
                    $osServiceOrder->server = $server;
                    $osServiceOrder->service = $osService;
                    $osServiceOrder->date_expired = null;
                    $osServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                    $osServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                    $osServiceOrder->description = $osService->getTranslates($language)->name;
                    $em->persist($osServiceOrder);
                    $osServiceOrder->setName($osServiceOrder->id);
                    /** @var Server $server */
                    $server = $server->addOrder($osServiceOrder);
                }

                if ($ipService instanceof Service) {
                    $ipServiceOrder = new Order();
                    $ipServiceOrder->status = Order::STATUS_PENDING;
                    $ipServiceOrder->currency = $order->currency;
                    $ipServiceOrder->server = $server;
                    $ipServiceOrder->service = $ipService;
                    $ipServiceOrder->date_expired = null;
                    $ipServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                    $ipServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                    $ipServiceOrder->description = $ipService->getTranslates($language)->name;
                    $em->persist($ipServiceOrder);
                    $ipServiceOrder->setName($ipServiceOrder->id);
                    /** @var Server $server */
                    $server = $server->addOrder($ipServiceOrder);
                }
            }
            /** @var Order $order */
            $order = $server->getMainOrder();
            $prices = $order::calculateAllPricesByOrder($order, $currency);

            if ($next !== null) {
                if (
                    $region instanceof Region and
                    $currency instanceof Currency and
                    $rac instanceof RAC and
                    $cpuService->cpu instanceof CPU and
                    $ramService->ram instanceof RAM and
                    $hddService->hdd instanceof HDD and
                    $osService->os instanceof OperationSystem and
                    $portService->bandwidth instanceof Bandwidth and
                    $server->ip instanceof IP and
                    $server->ipmi_ip instanceof IP and
                    $server->ip->ip !== null and
                    $server->ipmi_ip->ip !== null
                ) {
                    $data = $request->request->all();
                    unset($data['next']);
                    if (!$ipService instanceof Service) {
                        unset($data['ipService']);
                    }
                    foreach ($data as $field => $value) {
                        $session->set($field, $value);
                    }
                    /** @var User $user */
                    $user = $this->getUser();
                    if ($user instanceof User and $user->hasRole(User::ROLE_USER)) {
                        return new JsonResponse($this->generateUrl('gth_user_custom_server_confirm'));
                    }
                    return new JsonResponse($this->generateUrl('gth_user_custom_server_auth'));
                }
                return new JsonResponse($translator->trans('user.server.session.data.missing.error'), 409);
            }

            return $this->render('@GTHUser/CustomServer/prepare.html.twig', [
                'order' => $order,
                'server' => $server,
                'currencies' => $currencies,
                'lang' => $language,
                'osServices' => $osServices,
                'osService' => $osService,
                'bandwidthServices' => $portServices,
                'bandwidthService' => $portService,
                'ipServices' => $ipServices,
                'ipService' => $ipService,
                'prices' => $prices,
                'regions' => $regions,
                'racs' => $racs,
                'cpuServices' => $cpuServices,
                'cpuService' => $cpuService,
                'hddServices' => $hddServices,
                'hddService' => $hddService,
                'ramServices' => $ramServices,
                'ramService' => $ramService,
                'requiredServices' => $requiredServices,
            ]);
        }
        return $this->render('GTHUserBundle:CustomServer:index.html.twig', [
            "content_html" => null,
            "order" => $order
        ]);
    }

    /**
     * @param Service[] $services
     * @param $type
     * @param $subType
     * @param Currency $currency
     * @param Region|null $region
     * @param RAC $rac
     * @return \GTH\CoreBundle\Entity\Service[]
     */
    private function sortServicesByTypes($services = [], $type, $subType, Currency $currency, Region $region = null, RAC $rac = null)
    {
        /** @var Service[] $sortedServices */
        $sortedServices = [];
        foreach ($services as $service) {
            if ($service->type === $type and $service->sub_type === $subType and $service->getPrices($currency, $region) instanceof ServicePrice) {
                if ($rac instanceof RAC and $service->type === Service::TYPE_HARDWARE) {
                    if ($service->sub_type === Service::SUBTYPE_CPU and $service->cpu instanceof CPU and $service->cpu->getRac($rac) instanceof CPURac) {
                        $sortedServices[] = $service;
                    }
                    if ($service->sub_type === Service::SUBTYPE_RAM and $service->ram instanceof RAM and $service->ram->getRac($rac) instanceof RAMRac) {
                        $sortedServices[] = $service;
                    }
                    if ($service->sub_type === Service::SUBTYPE_HDD and $service->hdd instanceof HDD and $service->hdd->getRac($rac) instanceof HDDRac) {
                        $sortedServices[] = $service;
                    }
                } else {
                    $sortedServices[] = $service;
                }
            }
        }
        return $sortedServices;
    }

    public function authAction(Request $request)
    {
        if ($this->isGranted(User::ROLE_USER)) {
            return $this->redirectToRoute('gth_user_custom_server_confirm');
        }
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var TokenStorageInterface $tokenStorage */
        $tokenStorage = $this->get('security.token_storage');
        /** @var Translator $trans */
        $trans = $this->get('translator');
        /** @var int $maxAttempts */
        $maxAttempts = $this->getParameter('sign_in')['amount'];
        /** @var int $bannedTime */
        $bannedTime = $this->getParameter('sign_in')['minutes'];

        /** @var UserRepository $userRepository */
        $userRepository = $em->getRepository('GTHCoreBundle:User');

        $email = null;
        /** @var Language $language */
        $language = $this->getCurrentLanguage($request);
        if ($request->isMethod(Request::METHOD_POST)) {
            $email = $request->request->get('email');
            $password = $request->request->get('password');
            /** @var User $user */
            $user = $userRepository->loadUserByEmail($email, null, false, false);
            try {
                $userRepository->checkUser($user, $password, [User::ROLE_USER], true, true, true);
                $token = new UsernamePasswordToken($user, $user->getPassword(), "user", $user->getRoles());
                $tokenStorage->setToken($token);
                $request->setLocale($user->locale);
                $session = $request->getSession();
                $session->remove('token');
                $user->date_banned = null;
                $user->count_attemps = 0;
                $em->persist($user);
                $em->flush($user);
                return $this->redirectToRoute('gth_user_custom_server_confirm');
            } catch (\Exception $e) {
                if ($e->getCode() === 1) {
                    if ($user instanceof User && !is_null($user->date_banned)) {
                        $timestampDiff = $user->date_banned->getTimestamp()-time();
                        list($bannedMins, $bannedSecs) = [(int)($timestampDiff/60), $timestampDiff%60];
                        $this->addFlash('user.message.header.error', $trans->trans('user.message.banned_seconds', [':count' => $maxAttempts, ':minutes' => $bannedMins, ':seconds' => $bannedSecs]));
                    }
                } elseif ($e->getCode() === 2) {
                    if ($user instanceof User) {
                        $user->count_attemps++;
                        if ($user->count_attemps >= $maxAttempts && (is_null($user->date_banned) || $user->date_banned > new \DateTime())) {
                            $dateBanned = new \DateTime();
                            $dateBanned->add(new \DateInterval("PT" . $bannedTime . "M"));
                            $user->date_banned = $dateBanned;
                            /** @var Swift_Mailer $mailer */
                            $mailer = $this->get('mailer');
                            /** @var Swift_Message $message */
                            $message = $mailer->createMessage();
                            $message
                                ->addTo($user->email, $user->username)
                                ->addFrom($this->container->getParameter('mailer_sender_email'), $this->container->getParameter('mailer_sender_name'))
                                ->setSubject($trans->trans('user.message.banned.subject'))
                                ->setBody($trans->trans('user.message.banned.body', [':count' => $maxAttempts, ':minutes' => $bannedTime]), 'text/html', 'UTF-8');
                            if ($mailer->send($message)) {
                                $this->addFlash('user.message.header.error', $trans->trans('user.message.banned', [':count' => $maxAttempts, ':minutes' => $bannedTime]));
                            }
                        }
                        $em->persist($user);
                        $em->flush($user);
                    }
                    if(!$this->get('session')->getFlashBag()->has('user.message.header.error')){
                        $this->addFlash('user.message.header.error', $trans->trans('user.message.email_or_password_not_found'));
                    }
                }
            }
        }

        /** @var Server $server */
        $server = $this->prepareServer($request);
        /** @var Order $order */
        $order = $server->getMainOrder();
        if (
            !$server->region instanceof Region or
            !$order instanceof Order or
            !$order->currency instanceof Currency or
            !$server->rac instanceof RAC or
            !$server->os instanceof OperationSystem or
            !$server->bandwidth instanceof Bandwidth or
            !$server->ip instanceof IP or
            !$server->ipmi_ip instanceof IP or
            $server->ip->ip === null or
            $server->ipmi_ip->ip === null
        ) {
            $this->addFlash('user.message.header.error', $trans->trans('user.server.session.data.missing.error'));
            return $this->redirectToRoute('gth_user_custom_server_prepare');
        }
        foreach ($server->getCPUS() as $CPUInstalled) {
            if (!$CPUInstalled instanceof CPUInstalled or !$CPUInstalled->cpu instanceof CPU) {
                $this->addFlash('user.message.header.error', $trans->trans('user.server.session.data.missing.error'));
                return $this->redirectToRoute('gth_user_custom_server_prepare');
                break;
            }
        }
        foreach ($server->getRAMS() as $RAMInstalled) {
            if (!$RAMInstalled instanceof RAMInstalled or !$RAMInstalled->ram instanceof RAM) {
                $this->addFlash('user.message.header.error', $trans->trans('user.server.session.data.missing.error'));
                return $this->redirectToRoute('gth_user_custom_server_prepare');
                break;
            }
        }
        foreach ($server->getHDDS() as $HDDInstalled) {
            if (!$HDDInstalled instanceof HDDInstalled or !$HDDInstalled->hdd instanceof HDD) {
                $this->addFlash('user.message.header.error', $trans->trans('user.server.session.data.missing.error'));
                return $this->redirectToRoute('gth_user_custom_server_prepare');
                break;
            }
        }

        /** @var Service[] $requiredServices */
        $requiredServices = $server->getOrderByServiceType(Service::TYPE_OTHER, Service::SUBTYPE_REQUIRED);
        /** @var Service $bandwidthService */
        $bandwidthService = ($server->bandwidth instanceof Bandwidth and $server->bandwidth->service instanceof Service) ? $server->bandwidth->service : null;
        /** @var Service $osService */
        $osService = ($server->os instanceof OperationSystem and $server->os->service instanceof Service) ? $server->os->service : null;
        /** @var Order[] $ipOrders */
        $ipOrders = $server->getOrderByServiceType(Service::TYPE_NETWORK, Service::SUBTYPE_IP);
        /** @var Service $ipService */
        $ipService = null;
        if (count($ipOrders) === 1) {
            $ipService = $ipOrders[0]->service;
        }

        $prices = $order::calculateAllPricesByOrder($order, $order->currency);

        $pricesHTML = $this->renderView('@GTHUser/CustomServer/prices.html.twig', [
            "order" => $order,
            "server" => $server,
            "prices" => $prices,
            "requiredServices" => $requiredServices,
            "bandwidthService" => $bandwidthService,
            "osService" => $osService,
            "ipService" => $ipService,
            "lang" => $language
        ]);
        return $this->render('@GTHUser/CustomServer/auth.html.twig', [
            "order" => $order,
            "pricesHTML" => $pricesHTML,
            "email" => $email
        ]);
    }

    public function prepareServer(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Translator $translator */
        $translator = $this->get('translator');
        /** @var SessionInterface $session */
        $session = $request->getSession();

        /** @var ServerRepository $ipRepository */
        $ipRepository = $em->getRepository('GTHCoreBundle:Server\IP');
        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $em->getRepository('GTHCoreBundle:Currency');
        /** @var RegionRepository $regionRepository */
        $regionRepository = $em->getRepository('GTHCoreBundle:Region');
        /** @var ServiceRepository $serviceRepository */
        $serviceRepository = $em->getRepository('GTHCoreBundle:Service');
        /** @var ServerRepository $racRepository */
        $racRepository = $em->getRepository('GTHCoreBundle:Server\RACRegion');

        $regionId = (int)$request->request->get('region', $session->get('region', 0));
        $currencyId = (int)$request->request->get('currency', $session->get('currency', 0));
        $racId = (int)$request->request->get('rac', $session->get('rac', 0));
        $cpuServiceId = (int)$request->request->get('cpuService', $session->get('cpuService', 0));
        $hddServiceId = (int)$request->request->get('hddService', $session->get('hddService', 0));
        $ramServiceId = (int)$request->request->get('ramService', $session->get('ramService', 0));
        $osServiceId = (int)$request->request->get('osService', $session->get('osService', 0));
        $portServiceId = (int)$request->request->get('portService', $session->get('portService', 0));
        $ipServiceId = (int)$request->request->get('ipService', $session->get('ipService', -1));

        /** @var Language $language */
        $language = $this->getCurrentLanguage($request);
        /** @var Language $defaultLanguage */
        $defaultLanguage = $this->getCurrentLanguage($request, $this->getParameter('locale'));
        /** @var Currency[] $currencies */
        $currencies = $currencyRepository->findAll();
        /** @var Region[] $regions */
        $regions = $regionRepository->findBy(['active' => true], ['id' => Criteria::DESC]);
        foreach ($regions as $key => $_reg) {
            $racsInRegion = count($racRepository->getListByRegion($_reg, new RAC(), 'rac'));
            if ($racsInRegion === 0) {
                unset($regions[$key]);
                sort($regions);
            }
        }
        if (count($regions) === 0 or count($currencies) === 0) {
            return new JsonResponse($translator->trans('user.server.session.data.missing.error'), 409);
        }
        /** @var Currency $currency */
        $currency = $this->getEntityById($currencies, $currencyId);
        /** @var Region $region */
        $region = $regionRepository->findOneBy(['id' => $regionId,'active' => true]);
        if (!$region instanceof Region) {
            $region = reset($regions) || null;
        }
        if (!$region instanceof Region) {
            return new JsonResponse($translator->trans('user.server.session.data.missing.error'), 409);
        }
        /** @var RAC[] $racs */
        $racs = $racRepository->getListByRegion($region, new RAC(), 'rac');
        $osService = new Service();
        $portService = new Service();
        $ipService = null;
        $cpuService = new Service();
        $hddService = new Service();
        $ramService = new Service();
        $serverIPMIip = new IP();
        $serverIP = new IP();
        $rac = new RAC();
        $server = (new Server())->prepareCustomServer($region, $currency, $defaultLanguage, $em, $rac, $cpuService, $hddService, $ramService, $osService, $portService, $serverIP, $serverIPMIip);
        if (count($racs) > 0) {
            /** @var RAC $rac */
            $rac = $this->getEntityById($racs, $racId);
            /** @var ListResult $servicesList */
            $servicesList = $serviceRepository->getListByRegion($region, $language, null, null, ['st.name' => Criteria::ASC], [
                ["active", 'eq', true],
                ["orx", 'orxX', [
                    ['hcr.rac', 'eq', $rac->id],
                    ['hrr.rac', 'eq', $rac->id],
                    ['hhr.rac', 'eq', $rac->id]
                ]]
            ]);
            /** @var Service[] $services */
            $services = $servicesList->getResult();
            /** @var Service[] $cpuServices */
            $cpuServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_CPU, $currency, $region, $rac);
            /** @var Service[] $hddServices */
            $hddServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_HDD, $currency, $region, $rac);
            /** @var Service[] $ramServices */
            $ramServices = $this->sortServicesByTypes($services, Service::TYPE_HARDWARE, Service::SUBTYPE_RAM, $currency, $region, $rac);
            /** @var Service[] $osServices */
            $osServices = $this->sortServicesByTypes($services, Service::TYPE_SOFTWARE, Service::SUBTYPE_OSES, $currency, null);
            /** @var Service[] $ipServices */
            $ipServices = $this->sortServicesByTypes($services, Service::TYPE_NETWORK, Service::SUBTYPE_IP, $currency, $region);
            /** @var Service[] $portServices */
            $portServices = $this->sortServicesByTypes($services, Service::TYPE_NETWORK, Service::SUBTYPE_BANDWIDTH, $currency, $region);
            /** @var Service[] $requiredServices */
            $requiredServices = $this->sortServicesByTypes($services, Service::TYPE_OTHER, Service::SUBTYPE_REQUIRED, $currency, $region);
            /** @var ListResult $freeIPList */
            $freeIPList = $ipRepository->getList(new IP(), null, null, ["id" => Criteria::DESC], [
                "active" => false,
                "public" => true,
                "ipmi" => false,
                "order" => null,
                "v.region" => $region->id
            ]);
            //TODO: REMOVE WHEN IPMI IP WILL BE NOT REQUIRED FOR SERVER
            /** @var ListResult $freeIPMIIPList */
            $freeIPMIIPList = $ipRepository->getList(new IP(), null, null, ["id" => Criteria::DESC], [
                "active" => false,
                "public" => false,
                "ipmi" => true,
                "order" => null,
                "v.region" => $region->id
            ]);
            /** @var int $countFreeIp */
            $countFreeIp = $freeIPList->getCount();
            //TODO: REMOVE WHEN IPMI IP WILL BE NOT REQUIRED FOR SERVER
            /** @var int $countFreeIPMIIp */
            $countFreeIPMIIp = $freeIPMIIPList->getCount();
            //clear ips
            foreach ($ipServices as $key => $_ipService) {
                if ($_ipService->count > $countFreeIp) {
                    unset($ipServices[$key]);
                }
            }
            /** @var IP[] $freeIPs */
            $freeIPs = $freeIPList->getResult();
            /** @var IP[] $freeIPMIips */
            $freeIPMIips = $freeIPMIIPList->getResult();

            if (count($cpuServices) > 0) {
                /** @var Service $cpuService */
                $cpuService = $this->getEntityById($cpuServices, $cpuServiceId);
            }
            if (count($hddServices) > 0) {
                /** @var Service $hddService */
                $hddService = $this->getEntityById($hddServices, $hddServiceId);
            }
            if (count($ramServices) > 0) {
                /** @var Service $ramService */
                $ramService = $this->getEntityById($ramServices, $ramServiceId);
            }
            if (count($osServices) > 0) {
                /** @var Service $osService */
                $osService = $this->getEntityById($osServices, $osServiceId);
            }
            if (count($portServices) > 0) {
                /** @var Service $portService */
                $portService = $this->getEntityById($portServices, $portServiceId);
            }
            if ($countFreeIp > 0) {
                /** @var IP $serverIP */
                $serverIP = $freeIPs[0];
                $serverIP->active = true;
            }
            if ($countFreeIPMIIp > 0) {
                /** @var IP $serverIPMIip */
                $serverIPMIip = $freeIPMIips[0];
                $serverIPMIip->active = true;
            }
            if (count($ipServices) > 0) {
                /** @var Service|null $ipService */
                $ipService = $ipServiceId >= 0 ? $this->getEntityById($ipServices, $ipServiceId) : null;
            }

            /** @var Service|null $otherService */
            $otherService = null;
            $server = (new Server())->prepareCustomServer($region, $currency, $defaultLanguage, $em, $rac, $cpuService, $hddService, $ramService, $osService, $portService, $serverIP, $serverIPMIip);
            /** @var Order $order */
            $order = $server->getMainOrder();
            foreach ($requiredServices as $requiredService) {
                $requiredServiceOrder = new Order();
                $requiredServiceOrder->status = Order::STATUS_PENDING;
                $requiredServiceOrder->currency = $order->currency;
                $requiredServiceOrder->server = $server;
                $requiredServiceOrder->service = $requiredService;
                $requiredServiceOrder->date_expired = null;
                $requiredServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                $requiredServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                $requiredServiceOrder->description = $requiredService->getTranslates($language)->name;
                $em->persist($requiredServiceOrder);
                $requiredServiceOrder->setName($requiredServiceOrder->id);
                /** @var Server $server */
                $server = $server->addOrder($requiredServiceOrder);
            }

            if ((float)$portService->getPrices($order->currency, $server->region, true)->price > 0.00) {
                $portServiceOrder = new Order();
                $portServiceOrder->status = Order::STATUS_PENDING;
                $portServiceOrder->currency = $order->currency;
                $portServiceOrder->server = $server;
                $portServiceOrder->service = $portService;
                $portServiceOrder->date_expired = null;
                $portServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                $portServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                $portServiceOrder->description = $portService->getTranslates($language)->name;
                $em->persist($portServiceOrder);
                $portServiceOrder->setName($portServiceOrder->id);
                /** @var Server $server */
                $server = $server->addOrder($portServiceOrder);
            }

            if ((float)$osService->getPrices($order->currency, $server->region, true)->price > 0.00) {
                $osServiceOrder = new Order();
                $osServiceOrder->status = Order::STATUS_PENDING;
                $osServiceOrder->currency = $order->currency;
                $osServiceOrder->server = $server;
                $osServiceOrder->service = $osService;
                $osServiceOrder->date_expired = null;
                $osServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                $osServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                $osServiceOrder->description = $osService->getTranslates($language)->name;
                $em->persist($osServiceOrder);
                $osServiceOrder->setName($osServiceOrder->id);
                /** @var Server $server */
                $server = $server->addOrder($osServiceOrder);
            }

            if ($ipService instanceof Service) {
                $ipServiceOrder = new Order();
                $ipServiceOrder->status = Order::STATUS_PENDING;
                $ipServiceOrder->currency = $order->currency;
                $ipServiceOrder->server = $server;
                $ipServiceOrder->service = $ipService;
                $ipServiceOrder->date_expired = null;
                $ipServiceOrder->date_created = (new \DateTime())->setTime(0, 0, 0);
                $ipServiceOrder->date_updated = (new \DateTime())->setTime(0, 0, 0);
                $ipServiceOrder->description = $ipService->getTranslates($language)->name;
                $em->persist($ipServiceOrder);
                $ipServiceOrder->setName($ipServiceOrder->id);
                /** @var Server $server */
                $server = $server->addOrder($ipServiceOrder);
                /** @var IP[] $ipServiceIPs */
                $ipServiceIPs = array_slice($freeIPs, 1, $ipService->count);
                foreach ($ipServiceIPs as $ipServiceIP) {
                    $ipServiceIP->active = false;
                    $ipServiceIP->order = $ipServiceOrder;
                }
                $ipServiceOrder->addIPs($ipServiceIPs);
            }
            return $server;
        }
        return $server;
    }

    public function confirmAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var Translator $translator */
        $translator = $this->get('translator');
        /** @var SessionInterface $session */
        $session = $request->getSession();
        /** @var EmailSender $emailSender */
        $emailSender = $this->container->get('email.sender');

        /** @var StaticPageRepository $staticPageRepository */
        $staticPageRepository = $em->getRepository("GTHCoreBundle:StaticPage");
        /** @var ServerRepository $ipRepository */
        $ipRepository = $em->getRepository('GTHCoreBundle:Server\IP');
        /** @var VoucherRepository $voucherRepository */
        $voucherRepository = $em->getRepository('GTHCoreBundle:Voucher\Voucher');
        /** @var PaymentSystemRepository $paymentSystemRepository */
        $paymentSystemRepository = $em->getRepository('GTHCoreBundle:PaymentSystem');

        $regionId = $session->get('region');
        $racId = $session->get('rac');
        $currencyId = $session->get('currency');
        $cpuServiceId = $session->get('cpuService');
        $ramServiceId = $session->get('ramService');
        $hddServiceId = $session->get('hddService');
        $osServiceId = $session->get('osService');
        $portServiceId = $session->get('portService');
        $ipServiceId = $session->get('ipService');
        $voucherCode = $session->get('voucher');
        if ($regionId === null or $racId === null or $currencyId === null or $cpuServiceId === null or $ramServiceId === null or $hddServiceId === null or $osServiceId === null or $portServiceId === null) {
            $this->addFlash('user.message.header.error', $translator->trans('user.server.session.data.missing.error'));
            if ($this->isGranted(User::ROLE_USER)) {
                return $this->redirectToRoute('gth_user_custom_server_prepare');
            }
            return $this->redirectToRoute('gth_user_custom_server_auth');
        }
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem = $paymentSystemRepository->findOneBy(['active' => true]);
        if (!$paymentSystem instanceof PaymentSystem) {
            $this->addFlash('user.message.header.error', $translator->trans('user.server.session.data.missing.error'));
            if ($this->isGranted(User::ROLE_USER)) {
                return $this->redirectToRoute('gth_user_custom_server_prepare');
            }
            return $this->redirectToRoute('gth_user_custom_server_auth');
        }
        /** @var Voucher|null $voucher */
        $voucher = null;
        if (!empty($voucherCode)) {
            /** @var Voucher $voucher */
            $voucher = $voucherRepository->findOneBy(['code' => $voucherCode, 'active' => true]);

        }
        /** @var Server $server */
        $server = $this->prepareServer($request);
        if (!$server instanceof Server) {
            return new JsonResponse($translator->trans('user.server.session.data.missing.error'), 409);
        }
        /** @var Order $order */
        $order = $server->getMainOrder();

        if (
            !$order instanceof Order or
            !$order->currency instanceof Currency or
            !$server->os instanceof OperationSystem or
            !$server->bandwidth instanceof Bandwidth or
            !$server->ip instanceof IP or
            $server->ip->ip === null or
            !$server->region instanceof Region or
            !$server->rac instanceof RAC or
            count($server->cpus) === 0 or
            count($server->hdds) === 0 or
            count($server->rams) === 0 or
            //TODO: REMOVE WHEN IPMI IP WILL BE NOT REQUIRED FOR SERVER
            !$server->ipmi_ip instanceof IP or
            $server->ipmi_ip->ip === null
        ) {
            $this->addFlash('user.message.header.error', $translator->trans('user.server.session.data.missing.error'));
            if ($this->isGranted(User::ROLE_USER)) {
                return $this->redirectToRoute('gth_user_custom_server_prepare');
            }
            return $this->redirectToRoute('gth_user_custom_server_auth');
        }
        /** @var ListResult $freeIPList */
        $freeIPList = $ipRepository->getList(new IP(), null, null, ["id" => Criteria::DESC], [
            "active" => false,
            "public" => true,
            "ipmi" => false,
            "order" => null,
            "v.region" => $server->region->id
        ]);
        /** @var int $countFreeIp */
        $countFreeIp = $freeIPList->getCount();
        /** @var Service|null $ipService */
        $ipService = $server->getOrderByServiceType(Service::TYPE_NETWORK, Service::SUBTYPE_IP);
        if (!empty($ipServiceId) and $ipService instanceof Service or $countFreeIp === 0) {
            if ($countFreeIp === 0 or $ipService->count + 1 > $countFreeIp) {
                $this->addFlash('user.message.header.error', $translator->trans('user.server.session.data.missing.error'));
                if ($this->isGranted(User::ROLE_USER)) {
                    return $this->redirectToRoute('gth_user_custom_server_prepare');
                }
                return $this->redirectToRoute('gth_user_custom_server_auth');
            }
        }
        /** @var Currency $currency */
        $currency = $order->currency;
        /** @var Language $lang */
        $lang = $this->getCurrentLanguage($request);
        /** @var User $user */
        $user = $this->getUser();
        $pageTermsUrl = null;
        $pagePrivacyUrl = null;
        if (!$this->isGranted(User::ROLE_USER)) {
            if ($order instanceof Order) {
                return $this->redirectToRoute('gth_user_custom_server_auth');
            }
            return $this->redirectToRoute('gth_user_auth_login');
        }
        /** @var StaticPage $pageTerms */
        $pageTerms = $staticPageRepository->findOneBy(['type' => StaticPage::TYPE_TERMS, 'active' => true]);
        if ($pageTerms instanceof StaticPage) {
            $pageTermsUrl = $pageTerms->url;
        }
        /** @var StaticPage $pagePrivacy */
        $pagePrivacy = $staticPageRepository->findOneBy(['type' => StaticPage::TYPE_PRIVACY, 'active' => true]);
        if ($pagePrivacy instanceof StaticPage) {
            $pagePrivacyUrl = $pagePrivacy->url;
        }

        //calculate all prices
        $order->user = $user;
        $prices = $order::calculateAllPricesByOrder($order, $currency, null, $voucher);
        $taxes = $order->getOrderTax($user->profile->province, $prices['total'] - $prices['tax'] + $prices['voucherDiscount'], true);
        if ($request->isMethod(Request::METHOD_POST)) {
            $termsAndConditions = (bool)$request->request->get('terms_and_conditions', false);
            $privacyPolicy = (bool)$request->request->get('privacy_policy', false);

            if (!$termsAndConditions and $pageTerms instanceof StaticPage) {
                $this->addFlash('user.message.header.error', $translator->trans('error.terms_and_conditions'));
                return $this->redirectToRoute('gth_user_custom_server_confirm');
            }
            if (!$privacyPolicy and $pagePrivacy instanceof StaticPage) {
                $this->addFlash('user.message.header.error', $translator->trans('error.terms_and_conditions'));
                return $this->redirectToRoute('gth_user_custom_server_confirm');
            }
            if ($voucher instanceof Voucher) {
                /** @var VoucherPrice $voucherPrice */
                $voucherPrice = $voucher->getPrices($currency);
                if ($voucherPrice instanceof VoucherPrice and $voucher->getBalance($currency) > 0) {
                    if ($voucher->payments->count() > 0) {
                        foreach ($voucher->payments as $payment) {
                            if ($payment->order instanceof Order and $payment->order->status !== Order::STATUS_CANCELED and $payment->order->user instanceof User) {
                                if ((int)$payment->order->user->id !== (int)$user->id) {
                                    $voucher = null;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            $em->beginTransaction();
            $em->persist($server);
            $em->persist($server->ip);
            $em->persist($server->ipmi_ip);
            foreach ($server->cpus as $hardware) {
                $em->persist($hardware);
            }
            foreach ($server->hdds as $hardware) {
                $em->persist($hardware);
            }
            foreach ($server->rams as $hardware) {
                $em->persist($hardware);
            }
            foreach ($server->orders as $_order) {
                if ($_order->service instanceof Service and $_order->service->type === Service::TYPE_SERVER) {
                    $_order->voucher = $voucher;
                }
                $em->persist($_order);
                $_order->user = $user;
                $_order->setName($_order->id);

                $em->persist($_order);
                if ($_order->service->type === Service::TYPE_SERVER) {
                    $server->name = "Custom Server #" . $order->id;
                    $em->persist($_order->service);
                    foreach ($_order->service->prices as $servicePrice) {
                        $em->persist($servicePrice);
                    }
                    foreach ($_order->service->translates as $serviceTranslate) {
                        $serviceTranslate->name = "Custom Server #" . $order->id;
                        $serviceTranslate->description = "Custom Server #" . $order->id;
                        $em->persist($serviceTranslate);
                    }
                }
                if (count($_order->ips) > 0) {
                    foreach ($_order->ips as $ip) {
                        $ip->order = $_order;
                        $ip->active = true;
                        $em->persist($ip);
                    }
                }
            }
            if ($voucher instanceof Voucher) {
                if ((float)$prices['total'] === 0.00) {
                    foreach ($server->orders as $_order) {
                        $server->updateOrderById((int)$_order->id, $currency, $_order->service->getPrices($currency, $server->region)->price, 1, $_order->service->mounthly ? Order::STATUS_DONE : Order::STATUS_PAID, true);
                        $_order = $server->getOrderById((int)$_order->id);
                        $em->persist($_order);
                    }
                    $payment = new Payment();
                    $payment->type = Payment::TYPE_MONTHLY;
                    $payment->currency = $currency;
                    $payment->months = 1;
                    $payment->order = $order;
                    $payment->payment_system = $paymentSystem;
                    $payment->voucher = $voucher;
                    $payment->status = Payment::STATUS_PAID;
                    $payment->tax = $prices['current_tax'];
                    $em->persist($payment);
                    foreach ($server->orders as $_order) {
                        $paymentOrder = new PaymentOrder();
                        $paymentOrder->payment = $payment;
                        $paymentOrder->order = $_order;
                        $paymentOrder->amount = $_order->price;
                        $em->persist($paymentOrder);
                    }
                    $voucherHistory = new VoucherHistory();
                    $voucherHistory->currency = $currency;
                    $voucherHistory->price = $prices['voucherDiscount'];
                    $voucherHistory->payment = $payment;
                    $em->persist($voucherHistory);
                }
            }
            /** @var ConstraintViolation $err */
            $err = null;
            try {
                $em->getConnection()->commit();
                $em->flush();
            } catch (\Exception $e) {
                if ($em->getConnection()->isTransactionActive()) {
                    $em->rollback();
                    $em->close();
                }
                $err = new ConstraintViolation($translator->trans('error.database'), null, [], null, null, null);
            }
            if ($err instanceof ConstraintViolation) {
                $this->addFlash('user.message.header.error', $translator->trans('error.database'));
            } else {
                //clear session data
                $session->remove('region');
                $session->remove('rac');
                $session->remove('currency');
                $session->remove('cpuService');
                $session->remove('ramService');
                $session->remove('hddService');
                $session->remove('osService');
                $session->remove('portService');
                $session->remove('ipService');
                if ($voucher instanceof Voucher and (float)$prices['total'] === 0.00) {
                    $session->remove('voucher');
                    $this->addFlash('user.message.header.success', $translator->trans('user.payment.success'));
                    $emailSender->buyCustomServer($server, $lang, $request, $this);
                } else {
                    $this->addFlash('user.message.header.success', $translator->trans('express_server.confirm.success'));
                }
                return $this->redirectToRoute('gth_user_order_view', ['id' => $order->id]);
            }
        }
        return $this->render('@GTHUser/CustomServer/confirm.html.twig', [
            "order" => $order,
            "server" => $server,
            "prices" => $prices,
            "taxes" => $taxes,
            "pageTermsUrl" => $pageTermsUrl,
            "pagePrivacyUrl" => $pagePrivacyUrl,
            "lang" => $lang,
            "user" => $user,
            "voucher" => $voucher,
        ]);
    }

    public function removeAction(Request $request)
    {
        if (!$this->isGranted(User::ROLE_USER)) {
            throw new AccessDeniedHttpException();
        }
        /** @var Translator $translator */
        $translator = $this->get('translator');
        /** @var SessionInterface $session */
        $session = $request->getSession();

        if ($request->isMethod(Request::METHOD_POST)) {
            $voucher = $request->get('voucher', null);
            if (!empty($voucher)) {
                $session->remove('voucher');
            } else {
                $session->remove('ipService');
            }
            $this->addFlash('user.message.header.success', $translator->trans('express_server.auth.updated'));
            return new JsonResponse($this->generateUrl('gth_user_custom_server_confirm'));
        }
        throw new AccessDeniedHttpException();
    }

    public function voucherAction(Request $request)
    {
        if (!$this->isGranted(User::ROLE_USER)) {
            throw new AccessDeniedHttpException();
        }
        /** @var Translator $translator */
        $translator = $this->get('translator');
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        /** @var SessionInterface $session */
        $session = $request->getSession();

        /** @var VoucherRepository $voucherRepository */
        $voucherRepository = $em->getRepository('GTHCoreBundle:Voucher\Voucher');
        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $em->getRepository('GTHCoreBundle:Currency');

        /** @var User $user */
        $user = $this->getUser();
        if ($request->isMethod(Request::METHOD_POST)) {
            $code = $request->request->get('code');
            $currency = (int) $request->request->get('currency');
            /** @var Currency $currency */
            $currency = $currencyRepository->findOneBy(['id' => $currency]);
            /** @var Voucher $voucher */
            $voucher = $voucherRepository->findOneBy(['code' => $code, "active" => true]);
            if (!$voucher instanceof Voucher) {
                return new JsonResponse($translator->trans('user.voucher.not_found'), 409);
            }
            /** @var VoucherPrice $voucherPrice */
            $voucherPrice = $voucher->getPrices($currency);
            if (!$voucher instanceof Voucher or !$voucherPrice instanceof VoucherPrice or $voucher->getBalance($currency) <= 0) {
                return new JsonResponse($translator->trans('user.voucher.not_found'), 409);
            }
            if ($voucher instanceof Voucher) {
                /** @var Currency $voucherCurrency */
                $voucherCurrency = $voucher->getUsedCurrency();
                if ($voucherCurrency instanceof Currency and (int)$currency->id !== (int)$voucherCurrency->id) {
                    return new JsonResponse($translator->trans('user.voucher.not_found'), 409);
                }
            }
            if ($voucher->payments->count() > 0) {
                foreach ($voucher->payments as $payment) {
                    if ($payment->order instanceof Order and $payment->order->status !== Order::STATUS_CANCELED and $payment->order->user instanceof User) {
                        if ((int)$payment->order->user->id !== (int)$user->id) {
                            return new JsonResponse($translator->trans('user.voucher.not_found'), 409);
                            break;
                        }
                    }
                }
            }
            $session->set('voucher', $voucher->code);
            return new JsonResponse(null, 201);
        }
        throw new AccessDeniedHttpException();
    }

}