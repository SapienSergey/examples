<?php
namespace GTH\CoreBundle\Services;

use Doctrine\ORM\EntityManager;
use GTH\CoreBundle\Entity\Language;
use GTH\CoreBundle\Entity\Order;
use GTH\CoreBundle\Entity\PaymentSystem;
use GTH\CoreBundle\Entity\Server\Server;
use GTH\CoreBundle\Entity\User;
use GTH\CoreBundle\Entity\Voucher\Voucher;
use GTH\CoreBundle\Repository\PaymentRepository;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\PayerInfo;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class PayPalManager
{
    /** @var ContainerInterface */
    protected $container;

    /** @var EntityManager*/
    protected $em;

    /** @var  ApiContext */
    protected $apiContext;

    const currencies = ['AUD','BRL','CAD','CZK','DKK','EUR','HKD','HUF','ILS','JPY','MYR','MXN','NOK','NZD','PHP','PLN', 'GBP','RUB','SGD','SEK','CHF','TWD','THB','USD'];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        /** @var PaymentRepository $paymentsRepository */
        $paymentsRepository = $this->em->getRepository('GTHCoreBundle:PaymentSystem');
        /** @var PaymentSystem $paypal */
        $paypal = $paymentsRepository->findOneBy(['type' => PaymentSystem::TYPE_PAYPAL]);
        $paypalConfigs = $paypal->getMethodClientData();
        $clientId = array_key_exists('client_id', $paypalConfigs) ? $paypalConfigs['client_id'] : null;
        $secretKey = array_key_exists('secret_key', $paypalConfigs) ? $paypalConfigs['secret_key'] : null;
        if ($clientId !== null and $secretKey !== null) {
            $this->apiContext = new ApiContext(new OAuthTokenCredential($clientId, $secretKey));
        } else {
            $this->apiContext = null;
        }
        $log_dir = $this->container->get('kernel')->getLogDir();
        if ($this->apiContext !== null) {
            $this->apiContext->setConfig([
                'log.LogEnabled' => true,
                'log.FileName' => $log_dir.'/PayPal.log',
                'log.LogLevel' => 'FINE'
            ]);
        }
    }

    /**
     * @param $paymentId
     * @param $PayerID
     * @param Order $order
     * @param Language $language
     * @param Voucher|null $voucher
     * @return Payment|ConstraintViolationList
     */
    public function executePayment($paymentId, $PayerID, Order $order, Language $language, Voucher $voucher = null)
    {
        /** @var Translator $trans */
        $trans = $this->container->get('translator');
        $errors = new ConstraintViolationList();
        if ($this->apiContext === null) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.invalid_token', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if (!$order instanceof Order or !$order->server instanceof Server or !$order->user instanceof User or !$language instanceof Language) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if ($order instanceof Order and $order->getItemListForPayPal($language) === null) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        /** @var ItemList $itemList */
        $itemList = $order->getItemListForPayPal($language, $voucher);
        /** @var Transaction $transaction */
        $transaction = $order->getPayPalTransaction($language, $this->container, $itemList);
        if ($order instanceof Order and $itemList instanceof ItemList and !$transaction instanceof Transaction) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
//        return $transaction;
        if ($errors->count() === 0) {
            $payment = Payment::get($paymentId, $this->apiContext);
            if (!$payment instanceof Payment) {
                $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.get', [], 'messages', $language->short), null, [], null, null, null);
                $errors->add($err);
                return $errors;
            }
            $execution = new PaymentExecution();
            $execution->setPayerId($PayerID);
            $execution->addTransaction($transaction);
            try {
                $result = $payment->execute($execution, $this->apiContext);
                try {
                    $payment = Payment::get($paymentId, $this->apiContext);
                    return $payment;
                } catch (\Exception $ex) {
                    $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.get_executed_payment', [], 'messages', $language->short), null, [], null, null, null);
                    $errors->add($err);
                    return $errors;
                }
            } catch (\Exception $ex) {
                $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.execute', [], 'messages', $language->short), null, [], null, null, null);
                $errors->add($err);
                return $errors;
            }
        }
        return $errors;
    }

    /**
     * @param $successURL
     * @param $failedURL
     * @param Order $order
     * @param Language $language
     * @param bool $returnOnlyPayment
     * @param Voucher $voucher
     * @return Payment|ConstraintViolationList
     */
    public function prepare($successURL, $failedURL, Order $order, Language $language, $returnOnlyPayment = false, Voucher $voucher = null)
    {
        /** @var Translator $trans */
        $trans = $this->container->get('translator');
        $errors = new ConstraintViolationList();
        if ($this->apiContext === null) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.invalid_token', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if (!$order instanceof Order or !$order->server instanceof Server or !$order->user instanceof User or !$language instanceof Language) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if ($order instanceof Order and $order->getItemListForPayPal($language) === null) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        /** @var ItemList $itemList */
        $itemList = $order->getItemListForPayPal($language, $voucher);
        /** @var Transaction $transaction */
        $transaction = $order->getPayPalTransaction($language, $this->container, $itemList);
        if ($order instanceof Order and $itemList instanceof ItemList and !$transaction instanceof Transaction) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.order', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if ($errors->count() > 0) {
            return $errors;
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        /** Определим какой вид платежа у нас */

        $redirectUrls = new RedirectUrls();
        try {
            $redirectUrls->setReturnUrl($successURL)->setCancelUrl($failedURL);
        } catch (\Exception $e) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.invalid_url', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        if ($errors->count() > 0) {
            return $errors;
        }
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));
        if ($returnOnlyPayment) {
            return $errors->count() > 0 ? $errors : $payment;
        }
        try {
            $payment->create($this->apiContext);
            return $payment;
        } catch (\Exception $ex) {
            $err = new ConstraintViolation($trans->trans('user.payment.error.paypal.invalid_url', [], 'messages', $language->short), null, [], null, null, null);
            $errors->add($err);
        }
        return $errors;
    }

    public function isAvailableCurrencyCode($code)
    {
        $container = $this->container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        /** @var PaymentRepository $paymentsRepository */
        $paymentsRepository = $this->em->getRepository('GTHCoreBundle:PaymentSystem');
        /** @var PaymentSystem $paypal */
        $paypal = $paymentsRepository->findOneBy(['type' => PaymentSystem::TYPE_PAYPAL]);
        $paypalConfigs = $paypal->getMethodClientData();
        if($paypalConfigs['currency_checking']){
            return in_array($code, self::currencies);
        }
        return true;
    }
}