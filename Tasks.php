<?php

namespace GTH\APIBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use GTH\CoreBundle\Entity\API\SubTask;
use GTH\CoreBundle\Entity\API\Task;
use GTH\CoreBundle\Entity\Order;
use GTH\CoreBundle\Entity\Server\IP;
use GTH\CoreBundle\Entity\Server\Server;
use GTH\CoreBundle\Entity\User;
use GTH\CoreBundle\Repository\ServerRepository;
use GTH\CoreBundle\Repository\TaskManagerRepository;
use GTH\CoreBundle\Repository\TaskRepository;
use GTH\CoreBundle\Repository\Util\ListResult;
use GTH\CoreBundle\Services\EmailSender;
use GTH\CoreBundle\Services\TaskManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TasksController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var EmailSender $emailSender */
        $emailSender = $this->container->get('email.sender');
        
        $tasksAsArray = (bool)$this->container->getParameter('api_tasks_as_array');

        /** @var TaskManagerRepository $managerRep */
        $managerRep = $em->getRepository("GTHCoreBundle:API\\TaskManager");
        /** @var \GTH\CoreBundle\Entity\API\TaskManager $manager */
        $manager = $managerRep->getByHash($request->headers->get("Token", null));
        if (!$manager instanceof \GTH\CoreBundle\Entity\API\TaskManager) {
            throw new AccessDeniedHttpException();
        }
        if ($request->isMethod(Request::METHOD_GET)) {
            /** @var TaskRepository $rep */
            $rep = $em->getRepository("GTHCoreBundle:API\\Task");
            /** @var ListResult $tasksList */
            $tasksList = $rep->getList(new Task(), null, null, ["id" => Criteria::ASC], [
                ['task_manager', 'eq', $manager],
                ['status', 'orX', [
                    ['e.status', 'eq', Task::STATUS_PENDING],
                    ['e.status', 'eq', Task::STATUS_IN_PROGRESS],
                ]],
            ]);
            /** @var Task[] $tasks */
            $tasks = $tasksList->getResult();
            /** @var SubTask[] $subTasks */
            $subTasks = [];
            foreach ($tasks as $_task) {
                /** @var Server $taskServer */
                $taskServer = $_task->server;
                /** @var Order $taskOrder */
                $taskOrder = $taskServer->getMainOrder();
                if ($_task->client_id === null or !$taskOrder instanceof Order) {
                    if (!$taskServer instanceof Server) {
                        continue;
                    }
                    /** @var SubTask[] $_subTasks */
                    $_subTasks = [];
                    if ($taskServer->hasAllDoneTasks($_task) and $_task->client_id === null) {
                        $_subTasks = $_task->subtasks;
                    }
                    if ($_task->client_id !== null and $taskServer->hasAllDoneTasksByClient($this->container, $_task)) {
                        $_subTasks = $_task->subtasks;
                    }
                    foreach ($_subTasks as $_subTask) {
                        if ($_subTask->status === SubTask::STATUS_DONE) continue;
                        if ($_subTask->status === SubTask::STATUS_PENDING) break;
                        $_task->status = Task::STATUS_IN_PROGRESS;
                        $subTasks[] = $_subTask;
                        break;
                    }
                    if (count($subTasks) === 1 and !$tasksAsArray) break;
                    continue;
                }
                /** @var User $taskUser */
                $taskUser = $taskOrder->user;
                if (!$taskUser instanceof User) continue;
                if (!$taskUser->allTasksDone($_task)) continue;
                foreach ($_task->subtasks as $_subTask) {
                    if ($_subTask->status === SubTask::STATUS_DONE) continue;
                    if ($_subTask->status === SubTask::STATUS_PENDING) break;
                    $_task->status = Task::STATUS_IN_PROGRESS;
                    $subTasks[] = $_subTask;
                    break;
                }
                if (count($subTasks) === 1 and !$tasksAsArray) break;
            }

            $em->beginTransaction();
            $json = [];
            foreach ($subTasks as $subTask) {
                $subTask->status = SubTask::STATUS_PENDING;
                $subTask->date_status_new = new \DateTime();
                /** @var Task $subTaskTask */
                $subTaskTask = $subTask->task;
                if ($subTaskTask->date_status_pending === null) {
                    $subTaskTask->status = Task::STATUS_IN_PROGRESS;
                    $subTaskTask->date_status_pending = new \DateTime();
                }
                $em->persist($subTaskTask);
                $em->persist($subTask);
                $json[] = $subTask->convertToJSON($this->container);
            }
            try {
                $em->getConnection()->commit();
                $em->flush();
            } catch (\Exception $e) {
                if ($em->getConnection()->isTransactionActive()) {
                    $em->rollback();
                    $em->close();
                }
                throw new HttpException(500);
            }

            return new JsonResponse($json);
        }
        if ($request->isMethod(Request::METHOD_POST)) {
            $data = (array)$request->request->get('data', []);
            if (!array_key_exists('taskId', $data) or !array_key_exists('result', $data)) {
                throw new BadRequestHttpException();
            }
            $taskId = (int)$data['taskId'];
            $result = (int)$data['result'];
            $error = (bool)$result <= 0;
            $message = array_key_exists('message', $data) ? $data['message'] : null;

            /** @var TaskRepository $subTaskRep */
            $subTaskRep = $em->getRepository("GTHCoreBundle:API\\SubTask");
            /** @var SubTask $subTask */
            $subTask = $subTaskRep->find($taskId);
            if ($subTask instanceof SubTask) {
                /** @var Task $task */
                $task = $subTask->task;
                /** @var SubTask[] $subTasks */
                $subTasks = $task->subtasks;
                $countDoneSubTasks = 0;
                foreach ($subTasks as $_subTask) {
                    if ($_subTask->status === SubTask::STATUS_DONE) {
                        $countDoneSubTasks++;
                    }
                }
                if ($error) {
                    $task->status = Task::STATUS_FAILED;
                    $subTask->status = Task::STATUS_FAILED;
                }

                if (!$error and $result === 1 and $subTask->task_name === SubTask::TASK_SERVER_INSTALL) {
                    $subTask->status = SubTask::STATUS_IN_PROGRESS;
                    $countDoneSubTasks--;
                }
                if (!$error and $result === 2 and $subTask->task_name === SubTask::TASK_SERVER_INSTALL) {
                    $subTask->status = SubTask::STATUS_DONE;
                }
                if (!$error and $result === 1 and $subTask->task_name !== SubTask::TASK_SERVER_INSTALL) {
                    $subTask->status = SubTask::STATUS_DONE;
                }
                $subTask->answer = $data;
                $subTask->message = $message;
                if (count($subTasks) === ($countDoneSubTasks + 1) and $task->status !== Task::STATUS_FAILED) {
                    $task->status = Task::STATUS_DONE;
                }
                $task->date_status_progress = new \DateTime();
                if ($subTask->task_name === SubTask::TASK_SERVER_INSTALL) {
                    if ($subTask->status === SubTask::STATUS_IN_PROGRESS) {
                        $subTask->date_status_pending = new \DateTime();
                    }
                    if ($subTask->status === SubTask::STATUS_DONE) {
                        $subTask->date_status_progress = new \DateTime();
                    }
                } else {
                    $subTask->date_status_progress = new \DateTime();
                    $subTask->date_status_pending = new \DateTime();
                }


                /** @var Server $server */
                $server = $task->server;
                if ($subTask->status === SubTask::STATUS_DONE) {
                    switch ($subTask->task_name) {
                        case SubTask::TASK_SERVER_IPMI_OPERATOR_ADD;
                            $server->ipmi_login = $subTask->login;
                            $server->ipmi_password = $subTask->password;
                            if ($server->getMainOrder() instanceof Order and $server->getMainOrder()->user instanceof User) {
                                $emailSender->operatorAdd($server, $request, $this);
                            }
                            break;
                        case SubTask::TASK_NETWORK_NEW_DEDICATED_SERVER;
                            $server->mounted = true;
                            $server->status = Server::STATUS_MAINTANCE;
                            if ($server->getMainOrder() instanceof Order and $server->getMainOrder()->user instanceof User) {
                                $emailSender->mountServer($server, $request, $this);
                            }
                            break;
                        case SubTask::TASK_NETWORK_RELEASE_DEDICATED_SERVER;
                            $server->mounted = false;
                            $server->status = Server::STATUS_MAINTANCE;
                            break;
                        case SubTask::TASK_SERVER_POWER_ON;
                            $server->status = Server::STATUS_ONLINE;
                            break;
                        case SubTask::TASK_SERVER_POWER_RESET;
                            $server->status = Server::STATUS_ONLINE;
                            break;
                        case SubTask::TASK_SERVER_POWER_OFF;
                            $server->status = Server::STATUS_OFFLINE;
                            break;
                        case SubTask::TASK_NETWORK_ADD_SHAPER;
                            $server->bandwidth = $task->bandwidth;
                            break;
                        case SubTask::TASK_NETWORK_CHANGE_SHAPER;
                            $server->bandwidth = $task->bandwidth;
                            break;
                        case SubTask::TASK_SERVER_INSTALL;

                            $server->installed = true;
                            $server->os = $task->os;
                            $server->status = Server::STATUS_ONLINE;
                            if ($server->getMainOrder() instanceof Order and $server->getMainOrder()->user instanceof User) {
                                $emailSender->osInstall($server, $request, $this);
                            }
                            break;
                        case SubTask::TASK_NETWORK_REMOVE_IP;
                            if ($subTask->ip instanceof IP) {
                                /** @var IP $removingIP */
                                $removingIP = $subTask->ip;
                                if ($removingIP->server instanceof Server) {
                                    $server->ip = null;
                                }
                                $removingIP->order = null;
                                $removingIP->active = false;
                                $em->persist($removingIP);
                            }
                            break;
                        case SubTask::TASK_NETWORK_DELETE_SERVER;
                            $server->installed = false;
                            $server->os = null;
                            break;
                    }
                }
                if ($subTask->status === SubTask::STATUS_FAILED) {
                    switch ($subTask->task_name) {
                        case SubTask::TASK_NETWORK_NEW_DEDICATED_SERVER:
                            $server->mounted = false;
                            $server->status = Server::STATUS_MAINTANCE;
                            break;
                        case SubTask::TASK_SERVER_INSTALL :
                            $server->installed = false;
                            $server->status = Server::STATUS_OFFLINE;
                            break;
                    }
                }
                if ($subTask->status === SubTask::STATUS_IN_PROGRESS) {
                    switch ($subTask->task_name) {
                        case SubTask::TASK_SERVER_INSTALL;
                            if (!empty($subTask->login) and !empty($subTask->password)) {
                                $server->ipmi_login = $subTask->login;
                                $server->ipmi_password = $subTask->password;
                            }
                            if ($server->getMainOrder() instanceof Order and $server->getMainOrder()->user instanceof User) {
                                $emailSender->operatorAdd($server, $request, $this);
                            }
                            break;
                    }
                }
                $em->beginTransaction();
                $em->persist($server);
                $em->persist($task);
                $em->persist($subTask);
                try {
                    $em->getConnection()->commit();
                    $em->flush();
                } catch (\Exception $e) {
                    if ($em->getConnection()->isTransactionActive()) {
                        $em->rollback();
                        $em->close();
                    }
                    throw new HttpException(500);
                }
            } else {
                throw new BadRequestHttpException();
            }

        }
        return new JsonResponse(null, 201);
    }
}